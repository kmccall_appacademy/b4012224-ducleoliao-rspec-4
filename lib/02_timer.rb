class Timer
  attr_accessor :seconds
  def initialize(seconds=0)
    @seconds=seconds
  end
  def seconds=(value)
    @seconds=value
  end
  def padded(n)
    return '%02d' % n
  end
  def time_string
    minutes = (seconds %  3600) / 60
    hours = seconds / 3600
    t_seconds = seconds % 60
    "#{padded(hours)}:#{padded(minutes)}:#{padded(t_seconds)}"
  end
end
