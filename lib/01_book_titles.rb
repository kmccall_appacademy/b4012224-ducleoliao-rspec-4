class Book
  attr_accessor :title
  def title=(title)
    exceptions = ["in","the", "of", "and", "an", "a"]
    cap = title.split.map {|word| exceptions.include?(word) ? word.downcase : word.capitalize}
    cap[0].capitalize!
    @title = cap.join(" ")
  end
end
