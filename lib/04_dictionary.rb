class Dictionary
attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(new_entry)
    if new_entry.is_a?(String)
      @entries[new_entry] = nil
    elsif new_entry.is_a?(Hash)
      @entries[new_entry.first.first] = new_entry.first.last
    end
  end

  def include?(keyword)
    @entries.any? { |kw, defintion| kw == keyword }
  end

  def find(keyword)
    @entries.select { |kw, definition| kw.include?(keyword) }
  end

  def keywords
    @entries.keys.sort
  end

  def printable
    printout = ""
    sorted = @entries.sort_by { |kw, define| kw }
    sorted.each do |kw, define|
      printout << "[#{kw}] \"#{define}\""
      printout << "\n" unless define == sorted.last.last
    end
    printout
  end
end
