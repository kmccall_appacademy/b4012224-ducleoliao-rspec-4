class Temperature

  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def self.from_celsius(temperature)
    self.new(c: temperature)
  end

  def self.from_fahrenheit(temperature)
    self.new(f: temperature)
  end

  def in_celsius
    @fahrenheit.nil? ? @celsius : (@fahrenheit - 32) * 5 / 9
  end

  def in_fahrenheit
    @celsius.nil? ? @fahrenheit : (@celsius * 9 / 5.0 ) + 32
  end
end

class Fahrenheit < Temperature

  def initialize(fahrenheit)
    @fahrenheit = fahrenheit
  end
end

class Celsius < Temperature

  def initialize(celsius)
    @celsius = celsius
  end
end
